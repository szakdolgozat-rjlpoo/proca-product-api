package xyz.nergal.proca.product.api;

import lombok.*;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProductResponse {

    private Long id;

    private String name;

    private String img;

    private String description;

    private Set<String> categories;

    private List<PriceResponse> prices;
}
