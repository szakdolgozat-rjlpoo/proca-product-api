package xyz.nergal.proca.product.api;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PriceChangeResponse {

    private Long productId;

    private PriceResponse priceResponse;
}
