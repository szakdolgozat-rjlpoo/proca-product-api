package xyz.nergal.proca.product.api;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PriceRequest {

    @NotEmpty
    @Size(min = 3, max = 3)
    private String currencyCode;

    @NotNull
    @Positive
    private BigDecimal value;
}
