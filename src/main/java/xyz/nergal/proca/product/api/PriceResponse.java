package xyz.nergal.proca.product.api;

import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PriceResponse {

    private Long id;

    private String currencyCode;

    private BigDecimal value;

    private String supplier;

    private LocalDateTime updatedAt;
}
