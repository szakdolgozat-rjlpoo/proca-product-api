package xyz.nergal.proca.product.api;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProductRequest {

    @NotEmpty
    private String name;

    private String img;

    private String description;

    @NotEmpty
    private Set<String> categories;
}
